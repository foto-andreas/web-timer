import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { PushNotificationsService } from 'ng-push';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  rest = 0;
  secs = 0;
  minutes = 10;
  secondsTimer = Observable.timer(0, 1000);
  timer = Observable.timer(this.period(), this.period());
  notifications = '?';

  private secSub: Subscription;
  private alarmSub: Subscription;

  private audio = new Audio('../assets/gong.wav');

  constructor(private _service: NotificationsService, protected _push: PushNotificationsService ) {
  }

  private period(): number {
    if (this.minutes < 1) {
      this.minutes = 1;
    }
    return this.minutes * 60 * 1000;
  }

  private alarmFun = (s) => {
    this.audio.play();
    this._service.warn(
      'Achtung! ' + new Date().toLocaleTimeString(),
      'Haltung kontrollieren!',
      {
        timeOut: 30000,
        showProgressBar: true,
        pauseOnHover: false,
        clickToClose: true,
        maxLength: 10
    });
    this._push.create('Haltung kontrollieren!',
      {
        sticky: true,
        icon: '../assets/icon_ruecken.png'
      })
      .subscribe(
        res => console.log(res),
        err => console.log(err)
    );
    this.secs = 0;
  }

  private secFun = (s) => {
    this.secs++;
    this.rest =  this.minutes * 60 - this.secs;
    switch (this._push.permission) {
      case 'default': this.notifications = 'unbestätigt';
      break;
      case 'granted': this.notifications = 'erlaubt';
      break;
      case 'denied': this.notifications = 'verboten';
    }
  }


  ngOnInit(): void {
    this._push.requestPermission();
    this.audio.load();
    this.secSub = this.secondsTimer.subscribe(this.secFun);
    this.alarmSub = this.timer.subscribe(this.alarmFun);
  }

  change() {
    this.secSub.unsubscribe();
    this.secs = 0;
    this.secSub = this.secondsTimer.subscribe(this.secFun);
    this.alarmSub.unsubscribe();
    this.timer = Observable.timer(this.period(), this.period());
    this.alarmSub = this.timer.subscribe(this.alarmFun);
  }

}
